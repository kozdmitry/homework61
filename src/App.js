import React, {useEffect, useState} from "react";
import './App.css';
import Countries from "./component/Countries/Countries";
import CountriesInfo from "./component/CountriesInfo/CountriesInfo";
import Form from "./Container/Form/Form";
import Country from "./component/Countries/Country";

const API_KEY = '52ebba995e1a5ce4fb1158ab03a0f191';


const App = () => {
        return (
            <div className="App">
                <Countries/>
                <CountriesInfo/>
                <Country/>
                <Form/>
            </div>
        );
};
export default App;
