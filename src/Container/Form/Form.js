import React from 'react';
import {useState} from "react";
import Weather from "../../component/Weather/Weather";
import "./Form.css";

const Form = () => {

    const API_KEY = '52ebba995e1a5ce4fb1158ab03a0f191';
    const [info, setInfo] = useState({});

    const getWeather = async (e) => {
        e.preventDefault();
        const addCity = e.target.elements.city.value;
        console.log(addCity);
            const url = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${addCity}&appid=${API_KEY}&units=metric`);
            const data = await url.json();
            console.log(data);
            setInfo({
                temp: data.main.temp,
                city: data.name,
                country: data.sys.country,
                pressure: data.main.pressure,
                error: ""
            });

    };
    return (
        <form onSubmit={getWeather} className="form">
            <input className="formInput" type="text" name="city" placeholder="Enter your City"/>
            <button type="submit">Посмотреть погоду</button>
            <Weather
                temp={info.temp}
                city={info.city}
                country={info.country}
                pressure={info.pressure}
                error={info.error}
            />
        </form>
    );

};

export default Form;