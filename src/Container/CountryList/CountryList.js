import React from 'react';
import {useEffect, useState} from "react/cjs/react.production.min";
import axios from "axios";
import CountriesInfo from "../../component/CountriesInfo/CountriesInfo";


const CountryList = () => {

    const [list, setList] = useState([]);
    const listUrl = 'https://restcountries.eu/rest/v2/all?fields=name%3Balpha3Code';

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(listUrl);
            setList(response.data);
        }
        fetchData().catch(console.error);
    }, []);

    return (
        <div>
            <CountriesInfo
            name={list.name}

            />
        </div>
    );
};

export default CountryList;