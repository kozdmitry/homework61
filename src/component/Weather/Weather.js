import React from 'react';
import "./Weather.css";

const Weather = (props) => {
    return (
        <div className={Weather}>
            <h3>City: {props.city}</h3>
            <h5>Temp: {props.temp}</h5>
            <p>Country: {props.country}</p>
            <p>Pressure: {props.pressure}</p>
         </div>
    );
};

export default Weather;