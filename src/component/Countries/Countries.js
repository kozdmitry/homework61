import React from 'react';
import {useState} from "react";

import "./Countries.css";
import Country from "./Country";
import axios from "axios";
import CountriesInfo from "../CountriesInfo/CountriesInfo";

const URL_COUNTRY = 'https://restcountries.eu/rest/v2/name/usa';

const Countries = () => {
        const [list, setList] = useState([]);
        const [country, setCountry] = useState([]);
        const [border, setBorder] = useState([]);

    const url = 'https://restcountries.eu/rest/v2/all?fields=name%3Balpha3Code';

    const getListCountries = async (e) => {
                e.preventDefault();
                const apiUrlCountries = await fetch(url);
                console.log(apiUrlCountries);
                const data = await apiUrlCountries.json();
                setList(data);
    };


        const getCountryInfo = async () => {
            const response = await axios.get(URL_COUNTRY)
            setCountry(response.data)
            let borders = [];
             setBorder(borders)
        };

    return (
        <div className="CountriesTable">
            <button onClick={getListCountries}>Get a list of countries</button>
            <ul>
                {list.map ((country, index)=>(
                    <Country
                        key={index}
                        name={country.name}
                        onClick={()=> getCountryInfo(country.alpha3Code)}
                    />

                ))}
            </ul>
            <div>
                {country.map ((info, index)=>(
                    <CountriesInfo
                    key={index}
                    name={info.name}/>
                ))}
            </div>
        </div>
    );
};

export default Countries;