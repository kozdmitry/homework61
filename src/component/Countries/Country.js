import React from 'react';
import "./Country.css";

const Country = (props) => {
    return (
        <li>
            <button className="btn" onClick={props.onClick}>{props.name}</button>
        </li>

    );
};

export default Country;