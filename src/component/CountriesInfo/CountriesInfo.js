import React from 'react';
import "./CountriesInfo.css";

const CountriesInfo = (props) => {
    return (
        <div className="CountriesInfo">
            <p>{props.name}</p>
        </div>
    );
};

export default CountriesInfo;